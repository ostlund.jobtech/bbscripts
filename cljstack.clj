#!/usr/bin/env bb

(ns cljstack
  (:require [babashka.process :as process]
            [clojure.string :as str]
            [babashka.cli :as cli]))

(def help-str "This program is used to print stack traces for Clojure processes.

USAGE:

There are two subcommands:

`filter <search terms ...>`
`loop <delay> <search terms ...>`

The `filter` subcommand will display only stack traces mentioning at least one of the search terms. If no search term is provided, it will display all stack traces.

The `loop` subcommand will every `<delay>` seconds display all stack traces mentioning at least one of the search terms. If no search term is provided, it will instead display all stack traces.

EXAMPLE - Display all stack traces:

   cljstack.clj filter

EXAMPLE - Display only stack traces mentioning 'clojure':

   cljstack.clj filter clojure

EXAMPLE - Display only stack traces mentioning 'jobtech' or 'datahike':

   cljstack.clj filter jobtech datahike 

EXAMPLE - Display every two seconds only stack traces mentioning 'jobtech' or 'datahike':

   cljstack.clj loop 2 jobtech datahike 

It is convenient to add a symbolic link from a directory on the PATH
to this script, for example

ln -s /home/jonas/local/bin/cljstack /home/jonas/prog/bbscripts/cljstack.clj")

(defn parse-ps-line [ps-line]
  (let [parts (into []
                    (comp (map str/trim) (remove empty?))
                    (str/split ps-line #"\s+"))]
    (when (some #(= "java" %) parts)
      (-> parts first))))

(defn java-pids []
  (->> (process/shell {:out :string} "ps" "-A")
       :out
       str/split-lines
       (keep parse-ps-line)))

(defn parse-thread-block [block]
  (let [[header state & stack-trace] block]
    (into {}
          (remove (comp nil? val))
          {:header header
           :state state
           :stack-trace stack-trace})))

(defn parse-jstack-output [jstack-sections]
  (let [[header smr-info & blocks] jstack-sections]
    {:header header
     :smr-info smr-info
     :thread-blocks (mapv parse-thread-block blocks)}))

(defn jstack [pid]
  (->> (process/shell {:out :string} "jstack" (str pid))
       :out
       str/split-lines
       (map (fn [line] (let [trimmed (str/trim line)]
                         (when (seq trimmed)
                           trimmed))))
       (partition-by some?)
       (remove (comp nil? first))
       parse-jstack-output))

(defn some-string-with-some-search-term? [strings search-terms]
  (seq (for [s strings
             term search-terms
             :when (str/includes? s term)]
         true)))

(defn jstack-blocks-with-search-terms [search-terms]
  (for [java-pid (java-pids)
        block (:thread-blocks (jstack java-pid))
        :when (or (empty? search-terms)
                  (some-string-with-some-search-term?
                   (:stack-trace block)
                   search-terms))]
    block))

;; input ({:cmds (filter jobtech clojure), :args (jobtech clojure), :rest-cmds (jobtech clojure), :opts {}, :dispatch [filter]})

(defn print-filtered [{:keys [args]}]
  (println "----- STACK TRACES:")
  (doseq [{:keys [stack-trace]} (jstack-blocks-with-search-terms args)
          :when (seq stack-trace)]
    (println "\nThread:")
    (doseq [line (reverse stack-trace)]
      (println (str "  " line)))))

(defn print-loop [{:keys [args]}]
  (let [[delay & terms] args
        delay (long (* 1000.0 (Double/valueOf delay)))]
    (loop [counter 0]
      (print-filtered {:args terms})
      (Thread/sleep delay)
      (recur (inc counter)))))



(defn print-help [_]
  (println help-str))

(def table [{:cmds ["filter"]
             :fn print-filtered}
            {:cmds ["loop"]
             :fn print-loop}
            {:cmds ["help"]
             :fn print-help}])

(defn -main [& args]
  (cli/dispatch table args))

(apply -main *command-line-args*)
