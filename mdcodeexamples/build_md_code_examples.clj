#!/usr/bin/env bb

(ns build-md-code-examples
  (:require [clojure.test :refer [run-tests is deftest]]
            [babashka.cli :as cli]
            [clojure.string :as str]
            [taoensso.timbre :as log]
            [babashka.process :refer [shell]]
            [clojure.spec.alpha :as spec]
            [clojure.edn :as edn]
            [babashka.fs :as fs]))

(def unlabeled-code-block-separator #"^```(\s*)$")
(def labeled-code-block-separator #"^```(.*\S+.*)$")

(defn parse-line [line]
  (let [clean-out (-> line (str/replace "*" "") str/trim)]
    (or (when (#{"Output:"} clean-out)
          [:output-header line])
        (when (#{"Error:"} clean-out)
          [:error-header line])
        (when-let [[_full ws] (re-matches unlabeled-code-block-separator line)]
          [:unlabeled-code-block-separator ws])
        (when-let [[_full label] (re-matches labeled-code-block-separator line)]
          [:labeled-code-block-separator label])
        [:other line])))

(defn line-of-type-pred [& types]
  (comp (set types) first))

(def code-block-separator? (line-of-type-pred :unlabeled-code-block-separator
                                              :labeled-code-block-separator))
(def other-line? (line-of-type-pred :other))
(def output-header? (line-of-type-pred :output-header))
(def error-header? (line-of-type-pred :error-header))

(spec/def ::code-block
  (spec/cat :begin code-block-separator?
            :body (spec/* (line-of-type-pred :output-header :other))
            :end code-block-separator?))

(spec/def ::full-example (spec/cat :example-code ::code-block
                                   :more-lines (spec/* other-line?)
                                   :output-header output-header?
                                   :output ::code-block
                                   :error (spec/? (spec/cat :more-lines (spec/* other-line?)
                                                            :error-header error-header?
                                                            :output ::code-block))))
(spec/def ::md-file (spec/* (spec/alt :code-block ::code-block
                                      :other-line other-line?
                                      :full-example ::full-example)))

(def language-tags {"clj" :clojure
                    "clojure" :clojure
                    "py" :python
                    "python" :python})

(defn parse-tag [tag]
  (-> tag
      str/trim
      language-tags))

(defn get-language [[tag data]]
  (case tag
    :code-block (-> data :begin second parse-tag)
    :full-example (-> data :example-code :begin second parse-tag)))

(defn language-code-block-map [parsed-md]
  (dissoc (->> parsed-md
               (filter (comp #{:code-block :full-example} first))
               (group-by get-language))
          nil))

(def output-format "___OUTPUT_%d___:")
(def output-pattern #"^___OUTPUT_(\d+)___:$")

(defn render-output-code [index body]
  (let [msg (format output-format index)]
    (into [(format "print(\"%s\")" msg)
           (format "print(\"%s\", file=sys.stderr)" msg)]
          (map second)
          body)))

(defn python-script [code-blocks]
  (str/join "\n"
            (into ["import sys"]
                  (mapcat (fn [[cbtype data]]
                            (case cbtype
                              :code-block (render-output-code (:index data) (:body data))
                              :full-example (render-output-code (:index data)
                                                                (-> data :example-code :body)))))
                  code-blocks)))

(defn parse-output-line [line]
  (if-let [[_ index] (re-matches output-pattern line)]
    [:output-header (Long/valueOf index)]
    [:line line]))

(defn println-body [body]
  (doseq [[_ line] body]
    (println line)))

(defn parse-output-string [output-string verbose]
  (let [parsed-output (->> output-string
                           str/split-lines
                           (map parse-output-line)
                           (spec/conform ::output))]
    (when (= ::spec/invalid parsed-output)
      (throw (ex-info "Failed to parse output" {:output output-string})))
    (when-let [init (and verbose (:init parsed-output))]
      (binding [*out* *err*]
        (println "Init data")
        (println-body init)))
    (into {}
          (keep (fn [{:keys [header body]}]
                  (when (and verbose (seq body))
                    (binding [*out* *err*]
                      (println "Body at" (second header))
                      (println-body body)))
                  [(second header) (mapv second body)]))
          (:outputs parsed-output))))

(defn render-example-outputs [shell-result]
  (reduce (fn [dst [out-key k v]]
            (assoc-in dst [k out-key] v))
          {}
          (for [out-key [:out :err]
                [k v] (parse-output-string (out-key shell-result)
                                           (= :err out-key))]
            [out-key k v])))

(defn replace-many [dst replacements]
  (reduce (fn [s [src dst]] (str/replace s src dst))
          dst
          replacements))

(defn python-runner [code-blocks config]
  (let [script (replace-many (python-script code-blocks)
                             (:replacements config))
        workpath (fs/create-temp-dir)
        script-file (fs/file workpath "script.py")
        _ (spit script-file script)
        result (shell {:dir workpath :out :string :err :string :continue :true} "python3" (str script-file))]
    (if (zero? (:exit result))
      (log/info "Successfully run Python script:" (str script-file))
      (log/error "Error in Python script:" (str script-file)))
    (render-example-outputs result)))

(def runners {:python python-runner})

(def line? (line-of-type-pred :line))

(spec/def ::output (spec/cat :init (spec/* line?)
                             :outputs (spec/*
                                       (spec/cat
                                        :header (line-of-type-pred :output-header)
                                        :body (spec/* line?)))))

(defn run-all-code-blocks [code config]
  (into {}
        (mapcat (fn [[language-key code-blocks]]
                  (when-let [runner (runners language-key)]
                    (runner code-blocks config))))
        (language-code-block-map code)))


(def script-runners {:python python-runner})


(defn enumerate-examples [parts]
  (let [counter (atom -1)]
    (into []
          (map (fn [[part-type part-data]]
                 [part-type (if (#{:full-example :code-block} part-type)
                              (assoc part-data :index (swap! counter inc))
                              part-data)]))
          parts)))

(defn parse-md [s]
  (let [conformed (->> s
                       str/split-lines
                       (map parse-line)
                       (spec/conform ::md-file))]
    (assert (not= ::spec/invalid conformed))
    (enumerate-examples conformed)))

(defn wrap-other [x]
  [:other x])

(defn update-outputs [dst new-data]
  (-> dst
      (assoc-in [:output :body]
                (mapv wrap-other (:out new-data)))
      (dissoc :error)
      (merge (when-let [err (seq (:err new-data))]
               {:error {:more-lines [[:other ""]]
                        :error-header [:error-header "*Error:*"]
                        :output 
                        {:begin [:unlabeled-code-block-separator ""]
                         :body (mapv wrap-other err)
                         :end [:unlabeled-code-block-separator ""]}}}))))

(defn provide-example-outputs [dst outputs]
  {:pre [(sequential? dst)
         (map? outputs)]}
  (into []
        (map (fn [[k data]]
               [k (or (when (= k :full-example)
                        (update-outputs data (outputs (:index data))))
                      data)]))
        dst))

(defn render-md-line [[k data]]
  (case k
    :other data
    :labeled-code-block-separator (str "```" data)
    :unlabeled-code-block-separator "```"
    :output-header data
    :error-header data))

(defn flatten-if-needed [x]
  (into []
        (mapcat (fn [x]
                  {:pre [(sequential? x)]}
                  (if (keyword? (first x))
                    [x]
                    x)))
        x))

(defn process-md-string [s config]
  (let [tree (parse-md s)
        outputs (run-all-code-blocks tree config)]
    (if (empty? outputs)
      s
      (->> outputs
           (provide-example-outputs tree)
           (spec/unform ::md-file )
           flatten-if-needed
           (map render-md-line)
           (str/join "\n")))))

(def default-config {:replacements []})

(spec/def ::replacements (spec/coll-of (spec/cat :src string?
                                                 :dst string?)))
(spec/def ::config (spec/keys :req-un [::replacements]))

(defn validate-config [config]
  (when-not (spec/valid? ::config config)
    (throw (ex-info "Invalid config" {:config config}))))

(defn cli-rebuild [cli-args]
  (log/info "Rebuild" cli-args)
  (let [{:keys [path config-file]} (:opts cli-args)
        config (merge default-config
                      (first (for [f [config-file "mdcodeconfig.edn"]
                                   :when f
                                   :when (fs/exists? f)]
                               (-> f slurp edn/read-string))))]
    (log/info "Config" config)
    (validate-config config)
    (doseq [f (file-seq (fs/file path))
            :when (fs/regular-file? f)
            :when (str/ends-with? (fs/file-name f) ".md")
            :let [src (slurp f)
                  dst (process-md-string src config)]
            :when (not= src dst)]
      (log/info "Rebuild" (str f))
      (spit f dst ))))

(defn cli-print-help [_]
  (println "USAGE:\n")
  (println "./build_md_code_examples.clj rebuild PATH")
  (println "./build_md_code_examples.clj help")
  (println "./build_md_code_examples.clj test"))

(def table [{:cmds ["rebuild"]
             :fn cli-rebuild
             :args->opts [:path]}
            {:cmds ["test"]
             :fn (fn [& _] (run-tests))}
            {:cmds ["help"]
             :fn cli-print-help}])



(defn -main [& args]
  (cli/dispatch table args))


(deftest test-parse
  (is (= (parse-line "   *Output:*** ")
         (parse-line "   *Output:*** ")))
  (is (= [:unlabeled-code-block-separator "   "]
         (parse-line "```   ")))
  (is (= [:labeled-code-block-separator "py"]
         (parse-line "```py")))
  (is (= [:output-header 99]
         (parse-output-line (format output-format 99))))
  (is (= [:line "xyz"]
         (parse-output-line "xyz"))))

(def example-md "
I suggest we start with an import:

```py
import json
```

Here is some data:
```py
print([1, 2, 3])
```
and we get the following:

*Output:*
```
```
Not bad.
")

(def example-md2 "This is bad:
```py
assert(False)
```
We get this.

*Output:*
```
```
*Error:*
```
```

This code will not run:

```py
print(119)
```

*Output:*
```
```
")

(def example-md3 "Try a substitution:
```py
print(\"Hej!\")
```

*Output:*
```
```")

(defn demo-parse []
  (-> "/home/jonas/prog/jobtech-taxonomy-api/docs/taxonomy/src/howto/python/versions.md"
      slurp
      (process-md-string default-config)))


(deftest test-parse-2
  (is (= [[:other-line [:other "asdf"]]]
         (spec/conform ::md-file [[:other "asdf"]])))
  (let [parsed (parse-md example-md)
        cbmap (language-code-block-map parsed)
        pyblocks (:python cbmap)]
    (is (= [[:other-line [:other ""]]
            [:other-line [:other "I suggest we start with an import:"]]
            [:other-line [:other ""]]
            [:code-block
             {:begin [:labeled-code-block-separator "py"],
              :body [[:other "import json"]],
              :end [:unlabeled-code-block-separator ""],
              :index 0}]
            [:other-line [:other ""]]
            [:other-line [:other "Here is some data:"]]
            [:full-example
             {:example-code
              {:begin [:labeled-code-block-separator "py"],
               :body [[:other "print([1, 2, 3])"]],
               :end [:unlabeled-code-block-separator ""]},
              :more-lines [[:other "and we get the following:"] [:other ""]],
              :output-header [:output-header "*Output:*"],
              :output
              {:begin [:unlabeled-code-block-separator ""],
               :end [:unlabeled-code-block-separator ""]},
              :index 1}]
            [:other-line [:other "Not bad."]]]
           parsed))
    (is (= [:python] (keys cbmap)))
    (is (= 2 (count pyblocks)))
    (is (= "import sys\nprint(\"___OUTPUT_0___:\")\nprint(\"___OUTPUT_0___:\", file=sys.stderr)\nimport json\nprint(\"___OUTPUT_1___:\")\nprint(\"___OUTPUT_1___:\", file=sys.stderr)\nprint([1, 2, 3])"
           (python-script pyblocks)))
    (is (= "
I suggest we start with an import:

```py
import json
```

Here is some data:
```py
print([1, 2, 3])
```
and we get the following:

*Output:*
```
[1, 2, 3]
```
Not bad." (process-md-string example-md default-config)))
    (is (string? (process-md-string example-md2 default-config)))
    (let [a (str/split-lines (process-md-string example-md2 default-config))
          b (str/split-lines (process-md-string example-md2 default-config))
          line-set (set a)
          diffs (remove (fn [[a b]] (= a b)) (map vector a b))]
      (is (= (count a) (count b)))
      (is (line-set "```"))
      (is (line-set "```py"))
      (is (= 1 (count diffs))))
    (is (= "Try a substitution:
```py
print(\"Hej!\")
```

*Output:*
```
Tjena!
```" (process-md-string example-md3 {:replacements [["Hej" "Tjena"]]})))
    (let [s "```py
print(3)
```"]
      (is (= s (process-md-string s default-config))))
    (is (= "```py\nimport json\n```\n\nOutput:\n```\n```"
           (process-md-string "```py
import json
```

Output:
```
Hej
```" default-config)))))

(deftest test-cli-fn
  (let [root (fs/create-temp-dir)
        f (fs/file root "sample.md")
        _ (spit f "Code:
```py
print(119)
```
Output:
```
```")
        _ (-main "rebuild" (str root))]
    (is (= "Code:
```py
print(119)
```
Output:
```
119
```" (slurp f)))))

(defn demo []
  (run-tests))



(apply -main *command-line-args*)
