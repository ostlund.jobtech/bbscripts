(ns reducers
  (:require [clojure.walk :refer [postwalk]]))

(defn normalize-data [data]
  (postwalk
   (fn [x]
     (cond
       (set? x) (into (sorted-set) x)
       (map? x) (into (sorted-map) x)
       :else x))
   data))

(normalize-data {:b 9 :a 4 :c #{110 9 3 -16}})
;; => {:a 4, :b 9, :c #{-16 3 9 110}}

