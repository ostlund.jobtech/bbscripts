(ns tax-api-tool
  (:require [babashka.http-client :as http-client]
            [cheshire.core :as cheshire]))

(def default-config {:base-url "https://taxonomy.api.jobtechdev.se"})

(defn config? [x]
  (and (map? x)
       (string? (:base-url x))))

(defn graphql-request [config query]
  {:pre [(config? config)
         (string? query)]}
  (http-client/get (str (:base-url config) "/v1/taxonomy/graphql")
                   {:query-params {"query" query}}))

(defn graphql-body [response]
  (-> response
      :body
      (cheshire/parse-string true)))

(defn versions [config]
  (-> config
      (graphql-request  "query MyQuery {
  versions {
    id
    timestamp
  }
}")
      graphql-body))

(defn concept-types [config version]
  (-> config
      (graphql-request (format "query MyQuery {
  concept_types(version: \"%d\") {
    id
    label_en
    label_sv
  }
}" version))
      graphql-body
      :data
      :concept_types))

(defn concept-count-of-type [config version concept-type]
  {:pre [(config? config)
         (number? version)
         (string? concept-type)]}
  (-> config
      (graphql-request (format "query MyQuery {
  concepts(version: \"%d\", type: \"%s\") {
    id
  }
}" version concept-type))
      graphql-body
      :data
      :concepts
      count))

(defn report-counts-per-type [config version types]
  (let [type-map (update-vals (group-by :id (concept-types config version))
                               first)]
    (println "| Typ | Antal |")
    (println "|---|---|")
    (doseq [t types]
      (println (format "| %s | %d |"
                       (:label_sv (type-map t))
                       (concept-count-of-type config version t))))))


;; curl -X GET 'https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphql?query=query%20MyQuery%20%7B%20%20total_concepts_count%20%7B%20%20%20%20count%20%20%7D%7D' -H "Accept: application/json" -H "Content-Type: application/json" -H "api-key: "
