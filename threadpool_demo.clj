(ns threadpool-demo)

(import '[java.util.concurrent
          TimeUnit
          ArrayBlockingQueue
          ThreadPoolExecutor])

;(def queue (ArrayBlockingQueue. 30))
;(def executor (ThreadPoolExecutor. 4 4 1 TimeUnit/MINUTES queue))
;(def state (atom []))
;(.execute executor (fn [] (swap! state conj :hej)))
;(println "State:" (deref state))


(defn executor [size]
  (ThreadPoolExecutor. size size 1 TimeUnit/MINUTES (ArrayBlockingQueue. 30)))

(defn make-build-job [context repo]
  (fn []
    (println "Build repo" (:path repo))
    (Thread/sleep 70)
    (.put (:result context) [:built repo])))

(defn make-clone-job [context repo]
  (fn []
    (println "Clone repo" (:path repo))
    (Thread/sleep 100)
    (.execute (:build context) (make-build-job context repo))))

(defn clone-and-build-repositories [repositories]
  (let [n (count repositories)
        result-queue (ArrayBlockingQueue. 30)
        clone-executor (executor 4)
        context {:clone-executor clone-executor
                 :build-executor (executor 4)
                 :result result-queue}
        _ (doseq [repo repositories]
            (.execute clone-executor (make-clone-job context repo)))
        results (vec (repeatedly n #(.take result-queue)))]
    results))

(def example-repositories (for [i (range 30)]
                            {:path (str "/tmp/repo" i)}))
