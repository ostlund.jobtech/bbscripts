#!/usr/bin/env bb

(ns pandoc
  (:require [babashka.process :as process]
            [clojure.string :as str]
            [babashka.fs :as fs]))

(defn strip-comment-from-line [line]
  (if-let [i (str/index-of line ";;")]
    (subs line 0 i)
    line))

(defn remove-comments [text]
  (->> text
       str/split-lines
       (map strip-comment-from-line)
       (str/join "\n")))

(defn strip-extension [filename]
  (if-let [i (str/last-index-of filename ".")]
    (subs filename 0 i)
    filename))

(defn convert-to-tmp [src-filename]
  (let [intermediate-filename (fs/create-temp-file {:suffix ".md"})
        result-file (fs/file "/tmp" (str (strip-extension
                                          (fs/file-name src-filename))
                                         ".pdf"))]
    (->> src-filename
         fs/file
         slurp
         remove-comments
         (spit (fs/file intermediate-filename)))
    (process/shell "pandoc" (str intermediate-filename)
                   "-o" (str result-file))
    (println "Wrote" (str result-file))))

(defn -main [& args]
  (convert-to-tmp (first args)))

(apply -main *command-line-args*)

(defn demo []
  (convert-to-tmp "/home/jonas/jonaswiki/bjorn/2024-04-23-high-level-plan-for-bjorn.md")

  (comment

    
    )
  )
