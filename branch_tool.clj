#!/usr/bin/env bb

(ns branch-tool
  (:require [babashka.fs :as fs]
            [babashka.process :refer [shell]]
            [clojure.string :as str]
            [babashka.cli :as cli]
            [clojure.test :refer [deftest is run-tests run-all-tests]])
  (:import [java.time Instant]))

(defn normalize-path [p]
  (-> p fs/absolutize fs/normalize))

(defn run-git-command [repo-path & cmds]
  (apply shell {:dir repo-path :out :string} cmds))

(defn current-branch-name [repo-path]
  (-> repo-path
      (run-git-command "git rev-parse --abbrev-ref HEAD")
      :out
      str/trim))

(def backup-branch-name-pattern #"^(\S+)-bak(\d+)$")

(defn parse-backup-branch-name [backup-branch-name]
  (when-let [[_ base counter] (re-matches backup-branch-name-pattern
                                          backup-branch-name)]
    {:base base
     :counter (Long/parseLong counter)}))

(defn render-backup-branch-name [{:keys [base counter]}]
  {:pre [(string? base)
         (number? counter)]}
  (format "%s-bak%04d" base counter))

(deftest test-backup-branch-name
  (is (= {:base "datahike2-dev", :counter 0}
         (parse-backup-branch-name "datahike2-dev-bak00")))
  (is (= "datahike2-dev-bak0119"
         (render-backup-branch-name {:base "datahike2-dev", :counter 119}))))

(def branch-list-item-pattern #"TYPE(.) BRANCH(\S+) COMMIT(\S+) SUBJECT(.*) EMAIL(.*) TIMESTAMP(\S+)")


(defn parse-branch-list-item [line]
  (let [[_ branch-type branch commit subject email timestamp] (re-matches branch-list-item-pattern line)]
    {:branch-type (case branch-type
                    " " :other-branch
                    "*" :current-branch)
     :branch-name branch
     :commit commit
     :subject subject
     :email email
     :timestamp (Instant/parse timestamp)}))

(defn list-detailed-branches [repo-path]
  (->> (run-git-command repo-path "git for-each-ref --sort=committerdate refs/heads/ --format='TYPE%(HEAD) BRANCH%(refname:short) COMMIT%(objectname:short) SUBJECT%(contents:subject) EMAIL%(authoremail) TIMESTAMP%(committerdate:iso-strict)'
")
       :out
       str/split-lines
       (into [] (map parse-branch-list-item))))

(defn analyze-repository [repo-path]
  (let [repo-path (fs/absolutize repo-path)
        branches (list-detailed-branches repo-path)
        current-branch (first (filter #(= :current-branch (:branch-type %)) branches))
        max-backup-counter (transduce (keep (fn [x]
                                              (when-let [parsed (parse-backup-branch-name (:branch-name x))]
                                                (when (= (:branch-name current-branch) (:base parsed))
                                                  (:counter parsed)))))
                                      max
                                      -1
                                      branches)]
    (merge {:repo-path repo-path
            :repo-name (fs/file-name repo-path)
            :current-branch (:branch-name current-branch)
            :max-backup-counter max-backup-counter}
           (when (parse-backup-branch-name (:branch-name current-branch))
             {:error :current-is-backup
              :message (format "Current branch '%s' is a backup branch" (:branch-name current-branch))}))))

(defn perform-backup-for-repository [{:keys [repo-path max-backup-counter current-branch]
                                      :as analysis}]
  (let [next-branch-name (render-backup-branch-name {:base current-branch :counter (inc max-backup-counter)})
        _result (run-git-command repo-path "git branch" next-branch-name)]
    (merge analysis {:backup-branch next-branch-name})))

(defn format-backup-branch [b]
  (format "%s/`%s`" (:repo-name b) (:backup-branch b)))

(defn perform-backup [repo-paths]
  (let [analyses (into [] (map analyze-repository) repo-paths)
        errors (into [] (filter :error) analyses)]
    (if (seq errors)
      (do (println "ERRORS:")
          (doseq [e errors]
            (println (format "%s: %s"
                             (:repo-name e)
                             (:message e)))))
      (let [backups (into [] (map perform-backup-for-repository) analyses)]
        (case (count backups)
          0 nil
          1 (let [[b] backups]
              (println "[Backup branch](../topics/backup-branches.md):"
                       (format-backup-branch b)))
          (do 
            (println "[Backup branches](../topics/backup-branches.md):\n")
            (doseq [b backups]
              (println "*" (format-backup-branch b)))))))))

(defn cmd-backup [{:keys [args]}]
  (let [repos-to-backup (into [] (map normalize-path) args)
        non-existing (into [] (remove fs/exists?) repos-to-backup)]
    (cond
      (empty? repos-to-backup) (println "Nothing to backup")
      (seq non-existing) (do (println "Non-existing repos:")
                             (doseq [e non-existing]
                               (println "*" (str e))))
      :else (perform-backup repos-to-backup))))

;; https://stackoverflow.com/questions/5188320/how-can-i-get-a-list-of-git-branches-ordered-by-most-recent-commit


;; git for-each-ref --sort=committerdate refs/heads/ --format='%(HEAD) %(refname:short) - %(objectname:short) - %(contents:subject) - %(authorname) (%(committerdate:relative))'
;; git for-each-ref --sort=committerdate refs/heads/ --format='%(HEAD) %(refname:short) - %(objectname:short) - %(contents:subject) - %(authorname) (%(committerdate:iso))'

(def sample-list-item "TYPE  BRANCHdatahike2-dev-bak08 COMMIT7e2737eb SUBJECTwip EMAIL<ostlund.jobtech@gmail.com> TIMESTAMP2024-05-30T17:12:53+02:00")


(defn view-repos [args]
  (doseq [arg args]
    (let [repo-path (normalize-path arg)
          branches (list-detailed-branches repo-path)
          backup-map (group-by (fn [branch]
                                 (-> branch
                                     :branch-name
                                     parse-backup-branch-name
                                     :base))
                               branches)
          branches-to-list (for [item branches
                                 :let [branch-name (:branch-name item)]
                                 :when (not (parse-backup-branch-name branch-name))
                                 :let [backups (backup-map branch-name)]]
                             (assoc item :backups backups))
          width (transduce (map (comp count :branch-name)) max 0 branches-to-list)]
      (println (format "REPO %s:" (fs/file-name repo-path)))
      (doseq [item branches-to-list]
        (println (format (str "%s %s %s  %" width "s %s")
                         (case (:branch-type item)
                           :current-branch "*"
                           :other-branch " ")
                         (if (contains? #{"<ostlund.jobtech@gmail.com>"
                                          "<uppfinnarjonas@gmail.com>"}
                                        (:email item))
                           "jö"
                           "  ")
                         (str (:timestamp item))
                         (:branch-name item)
                         (if (seq (:backups item))
                           (format "%d backups" (count (:backups item)))
                           "")))))))

(defn cmd-view [{:keys [args]}]
  (view-repos args))

(def table [{:cmds ["backup"] :fn cmd-backup}
            {:cmds ["view"] :fn cmd-view}])

(defn -main [& args]
  (when args
    (cli/dispatch table args)))

(apply -main *command-line-args*)

(defn demo []
  (run-all-tests))

(comment

  (current-branch-name "/home/jonas/prog/jobtech-taxonomy-api")
  (list-detailed-branches "/home/jonas/prog/jobtech-taxonomy-api")
  (analyze-repository "/home/jonas/prog/jobtech-taxonomy-api")
  (perform-backup ["/home/jonas/prog/jobtech-taxonomy-api"
                   "/home/jonas/prog/datahike"])
  (cmd-view {:args ["/home/jonas/prog/datahike"]})

  (list-detailed-branches "/home/jonas/prog/jobtech-taxonomy-api")



  )
