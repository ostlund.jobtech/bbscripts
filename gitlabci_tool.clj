#!/usr/bin/env bb
;;
;; This is a Babashka script. Call
;;
;; ./gitlabci_tool.clj help
;;
;; to display help information.
;;
(ns gitlabci-tool
  (:require [clj-yaml.core :as yaml]
            [clojure.java.io :as io]
            [cheshire.core :as json]
            [clojure.string :as str]
            [babashka.process :refer [shell]]
            [babashka.http-client :as http-client]
            [flatland.ordered.map :refer [ordered-map]]
            [babashka.cli :as cli]
            [clojure.test :refer [deftest is run-tests]]
            [taoensso.timbre :as log])
  (:import [clojure.lang ExceptionInfo]
           [java.time Instant]
           [java.util UUID]))

(defmacro with-stderr [& body]
  `(binding [*out* *err*]
     ~@body))

(defn default-context []
  {:commit-id-length 40
   :cache-root (io/file (System/getProperty "user.home")
                        ".cache"
                        "gitlabci_tool")})

(defn read-cache-map [map-file]
  (if (.exists map-file)
    (-> map-file slurp (json/parse-string false))
    {}))

(defn write-cache-map [map-file cache-map]
  {:pre [(map? cache-map)
         (every? string? (keys cache-map))
         (every? string? (vals cache-map))]}
  (spit map-file (json/generate-string cache-map)))

(defn evaluate-cached-string [f input cache-root]
  {:pre [(string? input)
         cache-root]}
  (let [download-root (io/file cache-root "data")
        map-file (io/file cache-root "cached_files.json")
        cache-map (read-cache-map map-file)]
    (assert (every? string? (keys cache-map)))
    (assert (every? string? (vals cache-map)))
    (if-let [filename (cache-map input)]
      (slurp (io/file download-root filename))
      (when-let [output (f input)]
        (let [_ (assert (string? output))
              filename (str (UUID/randomUUID) ".json")
              full-file (io/file download-root filename)]
          (io/make-parents full-file)
          (spit full-file output)
          (write-cache-map map-file (assoc cache-map input filename))
          output)))))

(defn context-from-project-root [file]
  (let [file (io/file file)]
    (assoc (default-context)
           :name (.getName file)
           :gitlab-ci (io/file file ".gitlab-ci.yml")
           :project file
           :infra (-> file
                      (str "-infra")
                      io/file))))

(defn context-from-gitlab-ci [file]
  (-> file
      io/file
      .getParent
      context-from-project-root))

(defn context-from-infra-root [file]
  (let [file (str file)
        suffix "-infra"]
    (assert (str/ends-with? file suffix))
    (-> file
        (subs 0 (- (count file) (count suffix)))
        context-from-project-root)))

(defn context-from-x [x]
  (let [f (io/file x)]
    (cond
      (and (.isDirectory f) (str/ends-with? x "-infra")) (context-from-infra-root x)
      (.isDirectory f) (context-from-project-root x)
      (= ".gitlab-ci.yml" (.getName f)) (context-from-gitlab-ci x)
      :else (throw (ex-info "Cannot make context from x" {:x x})))))

(defn branch-name-from-list-item [item]
  (cond
    (empty? (str/trim item)) nil
    (str/starts-with? item "  ") (subs item 2)
    (str/starts-with? item "* ") (subs item 2)
    :else (throw (ex-info "Invalid list item" {:item item}))))

(defn try-run-git-command [repo-root & parts]
  (try
    (->> parts
         (remove nil?)
         (apply shell {:dir repo-root :out :string :err :string})
         :out)
    (catch ExceptionInfo _e
      nil)))

(defn get-commit-instant [repo-root commit-id]
  (when-let [epoch (try-run-git-command repo-root "git show -s --format=%ct" commit-id)]
    (->> epoch
         str/trim
         Long/parseLong
         Instant/ofEpochSecond)))

(defn git-rev-parse-short [repo-root len commit-ref]
  (when-let [s (try-run-git-command repo-root "git rev-parse" (format "--short=%d" len) commit-ref)]
    (str/trim s)))

(defn parse-branches [branches-str]
  (into []
        (keep branch-name-from-list-item)
        (str/split-lines branches-str)))

(defn get-branches-with-commit [repo-root commit-id is-remote]
  (when-let [branches (try-run-git-command repo-root "git branch --sort=committerdate --contains" commit-id (when is-remote "-r"))]
    (parse-branches branches)))

(def head-branch-pattern #"^.*HEAD.*:\s*(\S+)\s*$")

(defn parse-default-branch [s]
  (second (re-matches head-branch-pattern s)))

(defn default-branch [repo-root]
  (->> (try-run-git-command repo-root "git remote show origin")
       str/split-lines
       (keep parse-default-branch)
       first))

(defn branch-issues [default-branch branches]
  (let [on-default-branch (some #{default-branch} branches)]
    (cond
      (empty? branches) [{:message "Commit not found on any branch"}]
      (not on-default-branch) [{:message (format "Not found on %s branch, but on branches %s" default-branch (str/join "," branches))}]
      :else [])))

(defn commit-id-issues [{:keys [commit-id-length]} commit-id]
  {:pre [(number? commit-id-length)
         (string? commit-id)]}
  (let [actual-length (count commit-id)]
    (if (= actual-length commit-id-length)
      []
      [{:message (format "Expected commit id of length %d but actual length is %d"
                         commit-id-length actual-length)}])))

(defn read-yaml [file]
  (-> file
      slurp
      yaml/parse-string))

(defn overlays-root [context]
  (io/file (:infra context) "kustomize" "overlays"))

(defn overlay-root [context overlay-name]
  (io/file (overlays-root context) overlay-name))

(defn overlay-kustomization-file [context overlay-name]
  {:pre [(string? overlay-name)]}
  (io/file (overlay-root context overlay-name) "kustomization.yaml"))

(defn kustomize-build [repo-root]
  (-> (shell {:dir repo-root :out :string :err :string} "kustomize build .")
      :out))

(defn parse-kustomize [s]
  {:pre [(string? s)]}
  (let [sep #{"---"}]
    (into []
          (comp (partition-by sep)
                (remove (comp sep first))
                (map (fn [lines] (yaml/parse-string (str/join "\n" lines)))))
          (str/split-lines s))))

(defn kustomization-route-hosts [kustomization]
  (->> kustomization
       :route
       (keep (comp :host :spec))))

;; "docker-images.jobtechdev.se/jobtech-taxonomy-api/jobtech-taxonomy-api:de84c878"
(def docker-image-ref-pattern #"^(\S+):(\S+)$")

(defn parse-docker-image-ref [s]
  (when-let [[_ image-name image-tag] (re-matches docker-image-ref-pattern s)]
    {:name image-name
     :tag image-tag}))

(defn push-sub-nodes [stack context nodes]
  (into stack (map (fn [x] [context x])) nodes))

(defn scan-map [context node]
  (or (cond
        (contains? node :kind)
        (let [context (assoc context :object (select-keys node [:kind :metadata]))]
          [context {:namespace (-> node :metadata :namespace)}])

        (contains? node :image)
        (when-let [data (parse-docker-image-ref (:image node))]
          [context {:image data}]))
      [context {}]))

(defn scan-kustomization [kustomization]
  (loop [[[context node] & stack] [[{} kustomization]]
         output []]
    (if (nil? context)
      output
      (cond
        (sequential? node) (recur (push-sub-nodes stack context node) output)
        (number? node) (recur stack output)
        (string? node) (recur stack output)
        (boolean? node) (recur stack output)

        (map? node)
        (let [[context kv-pairs] (scan-map context node)]
          (assert (map? context))
          (assert (map? kv-pairs))
          (recur (push-sub-nodes stack context (vals node))
                 (into output (map (fn [[k v]] {:context context k v})) kv-pairs)))
        :else (throw (ex-info "Unexpected node" {:context context :node node}))))))

(defn expect-count-or-nil [n coll]
  (when (= n (count coll))
    (vec coll)))

(defn analyze-overlay-kustomization [context overlay-file kustomization]
  (let [found-values (scan-kustomization kustomization)]
    (if-let [[namespace] (expect-count-or-nil 1 (into #{} (keep :namespace) found-values))]
      (if-let [[{:keys [_name tag]}] (expect-count-or-nil 1 (into #{} (keep :image) found-values))]
        (let [commit-id tag
              repo-root (:project context)
              all-branches (for [is-remote [false true]
                                 branch-name (get-branches-with-commit
                                              repo-root commit-id is-remote)]
                             branch-name)
              commit-instant (get-commit-instant repo-root commit-id)
              default-branch (evaluate-cached-string default-branch
                                                     (.getAbsolutePath repo-root)
                                                     (io/file (:cache-root context) "default-branch"))]
          {:namespace namespace
           :commit {:id commit-id :instant commit-instant}
           :default-branch default-branch
           :branches all-branches
           :issues (into [] cat [(branch-issues default-branch all-branches)
                                 (commit-id-issues context commit-id)])
           :kustomization kustomization})
        (with-stderr (println "Failed to get image tag from kustomization:" overlay-file)))
      (with-stderr (println "Failed to get namespace from kustomization" overlay-file)))))

(defn analyze-overlay [context overlay-file]
  (let [overlay-name (.getName overlay-file)
        kustomization (->> overlay-file
                           kustomize-build
                           parse-kustomize
                           (group-by :kind)
                           (into {} (map (fn [[k v]] [(-> k str/lower-case keyword) v]))))]
    (when-let [analysis (analyze-overlay-kustomization
                         context
                         overlay-file
                         kustomization)]
      (merge {:name overlay-name
              :file overlay-file}
             analysis))))

(defn list-overlay-files [context]
  (-> context
      overlays-root
      .listFiles))

(defn list-overlays [context]
  (keep #(analyze-overlay context %) (list-overlay-files context)))

(defn deploy? [x]
  (and (map? x)
       (= ".deploy" (:extends x))))

(defn deploys
  ([src] (deploys src (constantly true)))
  ([src filter-fn]
   (into {} (for [[k v] src
                  :when (and (deploy? v) (filter-fn v))]
              [k v]))))

(defn deploy-overlay [d]
  (get-in d [:variables :OVERLAY]))

(defn normalize-includes [x]
  (if (map? x)
    [x]
    x))

(defn fetch-remote [context url]
  {:pre [(string? url)]}
  (yaml/parse-string
   (evaluate-cached-string (comp :body http-client/get)
                           url
                           (io/file (:cache-root context) "downloads"))))

(defn exclusion-file [{:keys [project]}]
  (io/file project ".gitlab-ci-exclusions.yml"))

(defn make-exclusions [ks]
  (into (ordered-map)
        (for [k ks]
          [k {:rules (list (ordered-map :if "$NON_EXISTING_VARIABLE == ''"))}])))

(defn remove-prefices [s prefices]
  (or (first (keep (fn [prefix]
                     (when (str/starts-with? s prefix)
                       (subs s (count prefix))))
                   prefices))
      s))

(defn overlay-base-name [context overlay-name]
  (-> overlay-name
      (remove-prefices [(str (:name context) "-")])
      (str/replace #"-" "_")))

(defn overlay-key [context overlay]
  (->> overlay
       :name
       (overlay-base-name context)
       (str "deploy_")
       keyword))

(defn inclusion-sort-feature [s incl]
  (if (str/includes? s incl) 0 1))

(defn overlay-sort-key [overlay]
  (let [s (:name overlay)]
    [(inclusion-sort-feature s "develop")
     (inclusion-sort-feature s "test")
     (inclusion-sort-feature s "read")
     (- (inclusion-sort-feature s "write"))
     (- (inclusion-sort-feature s "prod"))
     (- (inclusion-sort-feature s "af"))
     (- (inclusion-sort-feature s "onprem"))]))

(defn make-deploys-for-overlays [context overlays]
  (into (ordered-map)
        (for [overlay (sort-by overlay-sort-key overlays)]
          [(overlay-key context overlay)
           {:extends ".deploy"
            :variables {:OVERLAY (:name overlay)}}])))

(defn sequential-replace [pred replacements]
  (fn [step]
    (let [replaced (atom 0)
          try-step-replacements (fn [dst]
                                  (if (< 1 (swap! replaced inc))
                                    dst
                                    (reduce step dst replacements)))]
      (fn
        ([dst]
         (step (try-step-replacements dst)))
        ([dst x]
         (if (pred x)
           (try-step-replacements dst)
           (step dst x)))))))

(defn- get-remote-versions []
  (try
    (let [url-remote-tags "https://gitlab.com/api/v4/projects/43503997/repository/tags"
          json-response (http-client/get url-remote-tags)
          response (json/parse-string (:body json-response) true)]
      (mapv :name response))
    (catch ExceptionInfo _e
      nil)))

(defn- compare-versions [current remote-versions]
  (let [newer-vers (take-while (fn [v] (not= v current)) remote-versions)] 
    (when (seq newer-vers)
      (format "Consider upgrading remote ci script, currently using %s, but newer versions is available:\n%s\n" 
              current 
              (str/join "\n" newer-vers)))))

(defn- version-from-script-url [remote-ci-url]
  (re-find #"v[0-9]+\.[0-9]+\.[0-9]+" remote-ci-url))

(defn- versions-from-includes [includes]
  (into #{}
        (comp (keep :remote)
              (keep version-from-script-url))
        includes))

(defn- check-remote-script-version [current-versions]
  {:pre [(every? string? current-versions)]}
  (let [[current-version & other-versions] (seq current-versions)
        remote-versions (when current-version (get-remote-versions))]
    (cond
      (nil? current-version) "No current version detected."
      (nil? remote-versions) "No remote versions detected."
      other-versions "Multiple different versions detected."
      :else (compare-versions current-version remote-versions))))

(defn yaml-hacks [s]
  (str/replace s "!reference" "reference"))

(defn preprocess [context]
  (let [overlays (list-overlays context)
        overlay-map (into {} (map (juxt :name identity)) overlays)
        src (-> context
                :gitlab-ci
                slurp
                yaml-hacks
                yaml/parse-string)
        includes (normalize-includes (:include src))
        remote-deploys (into {}
                             (comp (keep :remote)
                                   (map #(fetch-remote context %))
                                   (mapcat deploys))
                             includes)
        deploys-with-overlays (deploys remote-deploys (comp overlay-map deploy-overlay))
        deploys-without-overlays (deploys remote-deploys (comp not overlay-map deploy-overlay))

        mentioned-overlays (into #{} (map deploy-overlay) (vals deploys-with-overlays))
        overlays-to-output (remove (comp mentioned-overlays :name) overlays)
        deploys-to-output (make-deploys-for-overlays context overlays-to-output)
        exfile (exclusion-file context)
        exname (.getName exfile)
        new-includes (into []
                           (sequential-replace #(= exname (:local %))
                                               [{:local exname}])
                           includes)
        new-source
        (into (ordered-map)
              (sequential-replace (comp deploy? second) deploys-to-output)
              (assoc src :include new-includes))
        excluded-keys (keys deploys-without-overlays)
        current-versions (versions-from-includes includes)
        version-issue (check-remote-script-version current-versions)]
    {:version-issue version-issue
     :excluded-keys excluded-keys
     :new-source new-source
     :exclusion-file exfile
     :deploys-with-overlays deploys-with-overlays
     :deploys-without-overlays deploys-without-overlays
     :deploys-to-output deploys-to-output
     :overlay-map overlay-map
     :overlays overlays
     :context context}))

(defn table-str
  ([rows] (table-str rows {}))
  ([rows {:keys [colsep]}]
   (let [colsep (or colsep "   ")
         widths (reduce (fn [widths row]
                          (let [widths2 (map count row)]
                            (if (nil? widths)
                              widths2
                              (do (assert (= (count widths2) (count widths)))
                                  (map max widths widths2)))))
                        nil
                        rows)]
     (str/join "\n" (for [row rows]
                      (str/join colsep (map (fn [x w]
                                              (apply str x (repeat (- w (count x)) " ")))
                                            row
                                            widths)))))))

(defn print-analysis [{:keys [overlay-map
                              overlays
                              deploys-without-overlays
                              deploys-with-overlays
                              deploys-to-output
                              version-issue]}]
  (let [all-issues (->> overlays
                        (into (ordered-map)
                              (comp (mapcat :issues)
                                    (map (fn [issue] [issue nil]))))
                        (into [] (map-indexed (fn [i [issue _]] [issue (inc i)]))))
        issue-to-index (into {} all-issues)
        print-deploy-list (fn [deploys]
                            (println (table-str (into [["KEY" "OVERLAY" "NAMESPACE" "TIMESTAMP" "COMMIT" "ISSUES"]]
                                                      (map (fn [[k v]]
                                                             (let [v (-> v deploy-overlay overlay-map)
                                                                   issues (:issues v)
                                                                   issue-message (if (empty? issues)
                                                                                   "-"
                                                                                   (str/join "," (map issue-to-index issues)))]
                                                               [(name k)
                                                                (:name v)
                                                                (:namespace v)
                                                                (-> v :commit :instant (or "-") str)
                                                                (-> v :commit :id)
                                                                
                                                                issue-message])))
                                                      deploys))))
        route-namespace-pairs (for [overlay overlays
                                    route (kustomization-route-hosts (:kustomization overlay))]
                                [route (:namespace overlay)])]
    (println "******************************")
    (println "*** gitlabci-tool analysis ***")
    (println "******************************\n")
    (when (seq route-namespace-pairs)
      (println "Defined routes:\n")
      (println (table-str (into [["ROUTE" "NAMESPACE"]] route-namespace-pairs) {:colsep "  ->  "}))
      (println ""))
    (when (seq deploys-without-overlays)
      (println "The following inherited deploys don't have corresponding overlays and can be excluded:")
      (doseq [[k _] deploys-without-overlays]
        (println "*" (name k)))
      (println ""))

    (when (seq deploys-with-overlays)
      (println "The following inherited deploys have corresponding overlays and can be retained:\n")
      (print-deploy-list deploys-with-overlays)
      (println ""))

    (when (seq deploys-to-output)
      (println "The following deploys can be generated in addition to the inherited ones:\n")
      (print-deploy-list deploys-to-output)
      (println ""))
    (when (seq all-issues)
      (println "Issues:")
      (doseq [[issue index] all-issues]
        (println (format "* %d: %s" index (:message issue)))))
    (when version-issue
      (println "Version issue:" version-issue))))

(defn generate-yaml [x]
  (yaml/generate-string x :dumper-options {:flow-style :block}))

(defn rebuild [{:keys [excluded-keys new-source exclusion-file context]}]
  (spit exclusion-file (generate-yaml (make-exclusions excluded-keys)))
  (spit (:gitlab-ci context) (generate-yaml new-source)))

(defn set-commit [{:keys [cli-input commit-id-length] :as context}]
  (let [opts (:opts cli-input)
        {:keys [overlay-name commit-id]} opts
        file (overlay-kustomization-file context overlay-name)
        _ (when (not (.exists file))
            (throw (ex-info "Missing kustomization.yaml file for overlay"
                            {:file (str file)
                             :overlay overlay-name})))
        src-overlay (update (read-yaml file) :images vec)

        old-tag (get-in src-overlay [:images 0 :newTag])
        _ (when (not (string? old-tag))
            (throw (ex-info "Old tag must be a string" {:src-overlay src-overlay})))
        commit-id (if (#{"HEAD"} commit-id)
                    (git-rev-parse-short (:project context) commit-id-length "HEAD")
                    commit-id)
        dst-overlay (assoc-in src-overlay [:images 0 :newTag] commit-id)
        _ (spit file (generate-yaml dst-overlay))
        analysis (analyze-overlay context (overlay-root context overlay-name))
        issues (:issues analysis)]

    (when (seq issues)
      (with-stderr
        (println "Issues after setting commit:")
        (doseq [issue issues]
          (println "*" (:message issue)))))))

(defn print-help [_]
  (println "USAGE:\n")
  (println "./gitlabci_tool.clj help")
  (println "./gitlabci_tool.clj analyze PATH")
  (println "./gitlabci_tool.clj rebuild PATH")
  (println "./gitlabci_tool.clj set-commit PATH OVERLAY COMMITID")
  (println "./gitlabci_tool.clj test")
  (println "\nThe PATH parameter is one of the following:")
  (println "  * The root directory, e.g. `jobtech-taxonomy-api`")
  (println "  * The infra directory, e.g. `jobtech-taxonomy-api-infra`")
  (println "  * The .gitlab-ci.yml file, that is. `.gitlab-ci.yml`")
  (println "From this path, all other paths will be derived.")
  (println "\nThe OVERLAY parameter is the name of an overlay, e.g. `jobtech-taxonomy-api-af-testing-read`")
  (println "\nThe COMMITID parameter is the hash of a commit, e.g. `de84c878`"))



(defn context-from-cli-input [x]
  (let [path (-> x :opts :path)]
    (assoc (context-from-x path)
           :cli-input x)))

(def table [{:cmds ["analyze"]
             :fn (comp print-analysis preprocess context-from-cli-input)
             :args->opts [:path]}
            {:cmds ["rebuild"]
             :fn (comp rebuild preprocess context-from-cli-input)
             :args->opts [:path]}
            {:cmds ["set-commit"]
             :fn (comp set-commit context-from-cli-input)
             :args->opts [:path :overlay-name :commit-id]}
            {:cmds ["test"]
             :fn (fn [& _] (run-tests))}
            {:cmds ["help"]
             :fn print-help}])

(defn -main [& args]
  (when (seq args)
    (cli/dispatch table args)))



(def example-kustomization
  {:configmap
   [{:apiVersion "v1",
     :data {:JDK_JAVA_OPTIONS "-Xmx7G -Xms3G"},
     :kind "ConfigMap",
     :metadata
     {:name "api-java-options-heap",
      :namespace "jobtech-taxonomy-api-onprem"}}
    {:apiVersion "v1",
     :data
     {:config.edn
      "{:options {:port 3000\n           :log-level :debug}\n :backends [\n            {:id :datahike\n             :type :datahike\n             :threads 1\n             :init-db {:wanderung/type :nippy-jar\n                   :filename \"taxonomy-v21.nippy\"}\n             :cfg {:store {:backend :mem\n                           :id \"in-mem-nippy-data\"}\n                   :wanderung/type :datahike\n                   :schema-flexibility :write\n                   :attribute-refs? true\n                   :index :datahike.index/persistent-set\n                   :keep-history true\n                   :name \"Nippy testing data\"}}]\n :jobtech-taxonomy-api {:auth-tokens {:111 :user}\n                        :user-ids {}}}\n"},
     :kind "ConfigMap",
     :metadata
     {:name "edn-config", :namespace "jobtech-taxonomy-api-onprem"}}
    {:apiVersion "v1",
     :data {:TAXONOMY_CONFIG_PATH "/mnt/config/config.edn"},
     :kind "ConfigMap",
     :metadata
     {:name "jobtech-taxonomy-api-config-path",
      :namespace "jobtech-taxonomy-api-onprem"}}],
   :service
   [{:apiVersion "v1",
     :kind "Service",
     :metadata
     {:labels {:app "taxonomy-app", :service "api"},
      :name "api",
      :namespace "jobtech-taxonomy-api-onprem"},
     :spec
     {:ports
      '({:name "3000-tcp",
        :port 3000,
        :protocol "TCP",
        :targetPort 3000}),
      :selector {:app "taxonomy-app", :service "api"},
      :type "NodePort"}}],
   :deployment
   [{:apiVersion "apps/v1",
     :kind "Deployment",
     :metadata
     {:labels {:app "taxonomy-app", :service "api"},
      :name "api",
      :namespace "jobtech-taxonomy-api-onprem"},
     :spec
     {:replicas 5,
      :selector {:matchLabels {:app "taxonomy-app", :service "api"}},
      :template
      {:metadata {:labels {:app "taxonomy-app", :service "api"}},
       :spec
       {:affinity
        {:podAntiAffinity
         {:preferredDuringSchedulingIgnoredDuringExecution
          '({:podAffinityTerm
            {:labelSelector
             {:matchLabels {:app "taxonomy-app", :service "api"}},
             :topologyKey "kubernetes.io/hostname"},
            :weight 1})}},
        :containers
        '({:envFrom
          ({:secretRef {:name "jobtech-taxonomy-api-secrets"}}
           {:configMapRef {:name "jobtech-taxonomy-api-config-path"}}
           {:configMapRef {:name "api-java-options-heap"}}),
          :image
          "docker-images.jobtechdev.se/jobtech-taxonomy-api/jobtech-taxonomy-api:e6306e4d",
          :imagePullPolicy "Always",
          :name "taxonomy",
          :ports ({:containerPort 3000}),
          :resources
          {:limits {:memory "7Gi"}, :requests {:memory "4Gi"}},
          :volumeMounts
          ({:mountPath "/mnt/config/config.edn",
            :name "config",
            :subPath "config.edn"})}),
        :volumes
        '({:configMap {:name "edn-config"}, :name "config"})}}}}]})

(deftest misc-tests
  (is (= [10 20 30 2 4 8] (into [] (sequential-replace odd? [2 4 8]) [10 20 30])))
  (is (= [10 2 4 8 20 30] (into [] (sequential-replace odd? [2 4 8]) [10 119 20 30])))
  (is (= [10 2 4 8 20 30 400] (into [] (sequential-replace odd? [2 4 8]) [10 119 20 30 1 3 5 400])))
  (is (= (make-exclusions [:abc])
         {:abc {:rules '[{:if "$NON_EXISTING_VARIABLE == ''"}]}}))
  (is (deploy? {:extends ".deploy"}))
  (is (not (deploy? {:extends "#deploy"})))
  (is (= "main" (parse-default-branch "    HEAD branch:     main     ")))
  (is (= "main" (parse-default-branch "    HEAD-gren:     main     "))))

(deftest kustomization-test
  (let [result (scan-kustomization example-kustomization)]
    (is (= 6 (count result)))
    (is (= (frequencies (map (comp set keys) (scan-kustomization example-kustomization)))
           {#{:context :namespace} 5
            #{:context :image} 1}))
    (is (= (into #{} (keep :namespace) result)
           #{"jobtech-taxonomy-api-onprem"}))
    (is (= (into #{} (keep :image) result)
           #{{:name
              "docker-images.jobtechdev.se/jobtech-taxonomy-api/jobtech-taxonomy-api",
              :tag "e6306e4d"}}))))

(deftest test-file-ops
  (let [tmpdir (System/getProperty "java.io.tmpdir")
        test-dir (io/file tmpdir (str (UUID/randomUUID)))
        cache-dir (io/file test-dir "cache")
        evaluation-counter (atom 0)
        f (fn [s] (swap! evaluation-counter inc) (str "*" s "*"))]
    (is (= "*bb*" (evaluate-cached-string f "bb" cache-dir)))
    (is (= 1 (deref evaluation-counter)))
    (is (= "*bb*" (evaluate-cached-string f "bb" cache-dir)))
    (is (= 1 (deref evaluation-counter)))
    (is (= "*xyz*" (evaluate-cached-string f "xyz" cache-dir)))
    (is (= 2 (deref evaluation-counter)))
    (is (= "*bb*" (evaluate-cached-string f "bb" cache-dir)))
    (is (= 2 (deref evaluation-counter)))
    (is (nil? (evaluate-cached-string (constantly nil) "x" cache-dir)))
    (is (= "*x*" (evaluate-cached-string f "x" cache-dir)))))

(deftest check-remote-script-version-test
  (is (nil? (version-from-script-url "asdf")))
  (is (= "No current version detected." (check-remote-script-version #{})))
  (is (= "v0.3.10"
         (version-from-script-url "https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/-/raw/v0.3.10/jobtech-ci.yml")))

  ;; This effectively tests that the `.gitlab-ci.yml` is up-to-date for this `ci-tools` project
  (is (contains?
       #{nil
         "No remote versions detected."} ;; <-- This can happen if we are offline.
       (-> ".gitlab-ci.yml"
           slurp
           yaml/parse-string
           :include
           normalize-includes
           versions-from-includes
           check-remote-script-version)))
  
  (is (= "Consider upgrading remote ci script, currently using v0.3.8, but newer versions is available:\nv0.3.10\nv0.3.9\n"
         (compare-versions "v0.3.8" ["v0.3.10" "v0.3.9"])))
  (is (= #{"v0.3.10"}
         (versions-from-includes
          '({:remote
             "https://gitlab.com/arbetsformedlingen/devops/gitlab-cicd/jobtech-ci/-/raw/v0.3.10/jobtech-ci.yml"}
            {:local ".gitlab-ci-exclusions.yml"}))))
  (is (= #{} (versions-from-includes '({:local "asdf"})))))


(comment

  (do
    (def project-name "openshift-varnish")
    (def project-dir (->> "user.dir"
                          System/getProperty
                          io/file
                          (iterate #(when % (.getParentFile %)))
                          (take-while some?)
                          (mapcat (fn [p] [p (io/file p project-name)]))
                          (filter #(and (.exists %) (= project-name (.getName %))))
                          first))
    (def context (context-from-x project-dir)))

   (print-analysis (preprocess context))


  (defn set-commit-demo
    ([] (set-commit-demo "abc1234"))
    ([commit-id]
     (set-commit
      (assoc context
             :cli-input {:opts {:overlay-name "develop"
                                :commit-id commit-id}}))))

  (run-tests)
  (def overlay-files (list-overlay-files context))
  (def overlays (vec (list-overlays context)))

  (def src (-> context :gitlab-ci slurp yaml/parse-string)))

(apply -main *command-line-args*)

