#!/usr/bin/env bb

(ns copy-files
  (:require [babashka.fs :as fs]
            [clojure.string :as str]))

(def default-list-file "/tmp/filelist.txt")

(defn list-files
  ([src-dir] (list-files src-dir default-list-file))
  ([src-dir dst-file]
   {:pre [(fs/path src-dir)
          (fs/path dst-file)]}
   (println "Writing file list to" dst-file)
   (spit (fs/file dst-file)
         (str/join
          "\n"
          (into []
                (map #(str (fs/relativize src-dir %)))
                (fs/list-dir src-dir))))))

(defn copy-files
  ([src-dir dst-dir]
   (copy-files src-dir dst-dir default-list-file))
  ([src-dir dst-dir list-file]
   {:pre [(fs/exists? src-dir)
          (fs/regular-file? list-file)]}
   (let [files (str/split-lines (slurp list-file))]
     (doseq [f files
             :let [src-file (fs/path src-dir f)
                   dst-file (fs/path dst-dir f)]
             :when (not (fs/exists? dst-file))]
       (println "Copy" (str src-file) "to\n    " (str dst-file))
       ((if (fs/regular-file? src-file)
          fs/copy
          fs/copy-tree)
        src-file dst-file {:replace-existing true}))
     (println "Done."))))

(defn -main [command & args]
  (case command
    "list" (apply list-files args)
    "copy" (apply copy-files args)
    nil))

(-main *command-line-args*)

(comment

  (do 
    (def rel ["Documents"])
    
    (def src-root (apply fs/path "/Volumes/JobTech/home/jonas" rel))
    (def dst-root (apply fs/path "/Users/osdjn" rel))
    )

  (list-files src-root)
  (copy-files src-root dst-root)


  )


