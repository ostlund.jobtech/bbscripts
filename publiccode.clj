#!/usr/bin/env bb

(ns publiccode
  (:require [babashka.http-client :as http-client]
            [babashka.pods :as pods]
            [clojure.string :as str]
            [clojure.walk :refer [postwalk]]
            [babashka.cli :as cli]
            [clojure.java.io :as io]
            [clojure.data.csv :as csv]))

(def root-url "https://standard.publiccode.net")

(def test-sub-url
  (str root-url "/criteria/document-codebase-maturity.html"))

(pods/load-pod 'retrogradeorbit/bootleg "0.1.9")

(require '[pod.retrogradeorbit.bootleg.utils :refer [convert-to]]
         '[pod.retrogradeorbit.hickory.select :as s])

(defn read-page [url]
  (-> url
      http-client/get
      :body
      str/trim
      (convert-to :hickory)))

(defn list-criteria [parsed-root]
  (let [criteria (atom [])]
    (postwalk (fn [x]
                (when (and (map? x)
                           (= :a (:tag x))
                           (string? (-> x :attrs :href))
                           (-> x :attrs :href (str/starts-with? "/criteria")))
                  (swap! criteria conj x))
                x)
              parsed-root)
    (deref criteria)))

(defn list-requirements [parsed-page]
  (let [reqs (atom [])
        req? (atom nil)]
    (postwalk (fn [x]
                (when (map? x)
                  (case (:tag x)
                    :h2 (reset! req? (-> x :attrs :id (= "requirements")))
                    :li (when (deref req?)
                          (swap! reqs conj x))
                    nil))
                x)
              parsed-page)
    (deref reqs)))

(defn flatten-content [x]
  (postwalk
   (fn [x]
     (cond
       (map? x) (apply str (:content x))
       (string? x) x
       :else x))
   x))

(defn scrape []
  (let [url root-url]
    (mapv (fn [crit]
            {:section (-> crit :content first)
             :requirements (->> crit
                                :attrs
                                :href
                                (str url)
                                read-page
                                list-requirements
                                (map flatten-content))})
          (list-criteria (read-page url)))))

;; (def data (scrape))

(defn strength [msg]
  (or (some #(when (str/includes? msg %) %)
            ["MUST"
             "SHOULD"
             "OPTIONAL"])
      ""))

(defn make-row
  ([]
   ["Section"
    "Requirement"
    "Strength"
    "Do we satisfy it? (Y/n/?)"
    "Can we satisfy it? (Y/n/?)"
    "Comment"])
  ([req] (make-row "" req))
  ([section req] [section req (strength req) "" "" ""]))

(defn csv-data [data]
  (into [(make-row)]
        (for [{:keys [section requirements]} data
              :let [[first-req & rest-reqs] requirements]
              rows [[(make-row section first-req)]
                    (mapv make-row rest-reqs)]
              row rows]
          row)))

(defn cli-scrape []
  (let [data (scrape)]
    (with-open [w (io/writer "/Users/osdjn/publiccode.csv")]
      (csv/write-csv w (csv-data data)))))

(defn -main [& args]
  (let [[action & args] args]
    (case action
      "scrape" (scrape)
      nil)))

(apply -main *command-line-args*)

