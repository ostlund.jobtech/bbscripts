#!/usr/bin/env bb

(ns clj-stack-profiler
  (:require [babashka.process :as process]
            [clojure.string :as str]
            [clojure.test :refer [deftest is run-tests]]
            [clojure.edn :as edn]
            [babashka.cli :as cli]))

(defn parse-ps-line [ps-line]
  (let [parts (into []
                    (comp (map str/trim) (remove empty?))
                    (str/split ps-line #"\s+"))]
    (when (some #(= "java" %) parts)
      (-> parts first))))

(defn java-pids []
  (->> (process/shell {:out :string} "ps" "-A")
       :out
       str/split-lines
       (keep parse-ps-line)))

(defn parse-thread-block [block]
  (let [[header state & stack-trace] block]
    (into {}
          (remove (comp nil? val))
          {:header header
           :state state
           :stack-trace stack-trace})))

(defn parse-jstack-output [jstack-sections]
  (let [[header smr-info & blocks] jstack-sections]
    {:header header
     :smr-info smr-info
     :thread-blocks (mapv parse-thread-block blocks)}))

(defn jstack [pid]
  (->> (process/shell {:out :string} "jcmd" (str pid) "Thread.print")
       :out
       str/split-lines
       (map (fn [line] (let [trimmed (str/trim line)]
                         (when (seq trimmed)
                           trimmed))))
       (partition-by some?)
       (remove (comp nil? first))
       parse-jstack-output))

(defn some-string-with-some-search-term? [strings search-terms]
  {:pre [(every? string? strings)]}
  (first (for [[i s] (map-indexed vector strings)
               term search-terms
               :when (str/includes? s term)]
           i)))

(defn jstack-blocks-with-search-terms
  "Use this to query the raw data"
  ([search-terms]
   (jstack-blocks-with-search-terms search-terms nil))
  ([search-terms pids]
   {:pre [(sequential? search-terms)
          (every? string? search-terms)]}
   (for [java-pid (or pids (java-pids))
         block (:thread-blocks (jstack java-pid))
         :let [line-limit (or (empty? search-terms)
                              (some-string-with-some-search-term?
                               (:stack-trace block)
                               search-terms))]
         :when line-limit]
     (assoc block
            :pid java-pid
            :limit line-limit))))

(defn step-counter [x]
  (update x :counter #(inc (or % 0))))

(defn step-path [dst path]
  (step-counter
   (if (empty? path)
     dst
     (let [[p & path] path]
       (update-in dst
                  [:sub (:source p)]
                  #(step-path
                    (merge % p)
                    path))))))

(def at-pattern #"^at (.*)\((.*)\:(.*)\)$")

(defn parse-class-and-method [s]
  (when-let [i (str/last-index-of s ".")]
    (let [cl (subs s 0 i)
          method (subs s (inc i))
          subclasses (str/split cl #"$")]
      {:qualified-class cl
       :method method
       :classes subclasses})))

(defn parse-stack-trace-line [s]
  (when-let [[_ class-and-method file line] (re-matches at-pattern s)]
    (merge {:source s
            :file file
            :line (Long/valueOf line)
            :qualified-name class-and-method}
           (parse-class-and-method class-and-method))))

(defn accumulate-sample [tree xform {:keys [limit pid stack-trace]}]
  (->> stack-trace
       (into []
             (comp (if (number? limit)
                     (take limit)
                     identity)
                   (keep parse-stack-trace-line)
                   xform))
       reverse
       (into [{:source pid :pid pid}])
       (step-path tree)
       step-counter))

(def default-setup {:search-terms []
                    :pids nil
                    :sample-count 100})

(defn setup? [x]
  (and (map? x)
       (contains? x :search-terms)))

(defn compute-node-times [data parent-counter parent-elapsed-seconds]
  (let [counter (:counter data)
        elapsed-seconds (/ (* counter parent-elapsed-seconds)
                           parent-counter)]
    (-> data
        (assoc :elapsed-seconds elapsed-seconds)
        (update :sub
                (fn [sub]
                  (update-vals
                   sub
                   #(compute-node-times
                     % counter elapsed-seconds)))))))

(defn block-data? [x]
  (and (map? x)
       (number? (:elapsed-ns x))
       (sequential? (:blocks x))))

(defn tree? [x]
  (and (map? x)
       (number? (:elapsed-seconds x))
       (map? (:sub x))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;;; A P I
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defn collect-blocks
  "Given a setup, collect profiling data. By default, 100 samples."
  [setup]
  (let [{:keys [search-terms pids sample-count]} (merge default-setup setup)
        start-ns (System/nanoTime)]
    (println "Search terms" search-terms)
    (loop [counter 0
           blocks []]
      (let [make-result (fn []
                          (let [end-ns (System/nanoTime)]
                            {:elapsed-ns (- end-ns start-ns)
                             :blocks blocks}))]
        (println (format "Collect sample %d/%d" counter sample-count))
        (if (<= sample-count counter)
          (make-result)
          (let [new-blocks (jstack-blocks-with-search-terms search-terms pids)]
            (if (empty? new-blocks)
              (make-result)
              (recur (inc counter)
                     (into blocks new-blocks)))))))))

(def remove-clojure-trace
  (remove (fn [{:keys [qualified-name]}]
            (when qualified-name
              (str/starts-with? qualified-name "clojure.")))))

(defn process-blocks
  ([block-data] (process-blocks block-data identity))
  ([block-data xform]
   (let [raw-tree (reduce (fn [dst block]
                            (accumulate-sample dst xform block))
                          {}
                          (:blocks block-data))]
     (compute-node-times raw-tree
                         (:counter raw-tree)
                         (* 1.0e-9 (:elapsed-ns block-data))))))

(defn render-text-tree
  ([tree] (render-text-tree tree ""))
  ([tree indent]
   {:pre [(tree? tree)]}
   (let [label (str (or (:qualified-name tree)
                        (:pid tree)
                        "root"))
         inner-indent (str indent "  ")]
     (println (format "%s%4d %.3f : %s"
                      indent
                      (:counter tree)
                      (:elapsed-seconds tree)
                      label))
     (doseq [[_k v] (sort-by (comp dec :counter val) (:sub tree))]
       (render-text-tree v inner-indent)))))








(comment ;; General

  (def block-data (collect-blocks {:search-terms ["exercise_many"]
                             :sample-count 100}))

  (spit "profdata.edn" (pr-str block-data))

  (def block-data (edn/read-string (slurp "profdata.edn")))
  
  (def tree (process-blocks block-data remove-clojure-trace))
  (spit "tree.txt" (with-out-str (render-text-tree tree)))
  )

(comment
  
  (def x (time (vec (jstack-blocks-with-search-terms ["exercise_many"] [107042]))))
  (def x (time (vec (jstack-blocks-with-search-terms ["exercise_many"]))))


  )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;
;;;; T E S T S
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def data0 [{:header
             "\"nREPL-session-6c960671-9bbe-4749-88a8-b94a9abc539e\" #35 [107094] daemon prio=5 os_prio=0 cpu=1506011,45ms elapsed=2013,06s tid=0x00007f2448003260 nid=107094 runnable  [0x00007f24cd2e5000]",
             :state "java.lang.Thread.State: RUNNABLE",
             :stack-trace
             ["at clojure.lang.RT.first(RT.java:692)"
              "at clojure.core$first__5449.invokeStatic(core.clj:55)"
              "at clojure.core$assoc__5481.invokeStatic(core.clj:197)"
              "at clojure.core$assoc__5481.doInvoke(core.clj:192)"
              "at clojure.lang.RestFn.invoke(RestFn.java:731)"
              "at datahike.debug_query$wrap_traced_fn$fn__70074.doInvoke(debug_query.clj:37)"
              "at clojure.lang.RestFn.invoke(RestFn.java:421)"
              "at datahike.query$lookup_pattern_db.invokeStatic(query.cljc:600)"
              "at datahike.query$lookup_pattern_db.invoke(query.cljc:597)"
              "at datahike.query$lookup_pattern.invokeStatic(query.cljc:642)"
              "at datahike.query$lookup_pattern.invoke(query.cljc:639)"
              "at clojure.lang.AFn.applyToHelper(AFn.java:165)"
              "at clojure.lang.AFn.applyTo(AFn.java:144)"
              "at clojure.core$apply.invokeStatic(core.clj:667)"
              "at clojure.core$apply.invoke(core.clj:662)"
              "at datahike.debug_query$wrap_traced_fn$fn__70074.doInvoke(debug_query.clj:37)"
              "at clojure.lang.RestFn.invoke(RestFn.java:457)"
              "at datahike.query$lookup_and_sum_pattern_rels$fn__36730.invoke(query.cljc:1154)"
              "at clojure.core$map$fn__5931$fn__5932.invoke(core.clj:2759)"
              "at clojure.lang.ArrayChunk.reduce(ArrayChunk.java:63)"
              "at clojure.core.protocols$fn__8244.invokeStatic(protocols.clj:136)"
              "at clojure.core.protocols$fn__8244.invoke(protocols.clj:124)"
              "at clojure.core.protocols$fn__8204$G__8199__8213.invoke(protocols.clj:19)"
              "at clojure.core.protocols$seq_reduce.invokeStatic(protocols.clj:31)"
              "at clojure.core.protocols$fn__8236.invokeStatic(protocols.clj:75)"
              "at clojure.core.protocols$fn__8236.invoke(protocols.clj:75)"
              "at clojure.core.protocols$fn__8178$G__8173__8191.invoke(protocols.clj:13)"
              "at clojure.core$transduce.invokeStatic(core.clj:6947)"
              "at clojure.core$transduce.invoke(core.clj:6933)"
              "at datahike.query$lookup_and_sum_pattern_rels.invokeStatic(query.cljc:1153)"
              "at datahike.query$lookup_and_sum_pattern_rels.invoke(query.cljc:1150)"
              "at clojure.lang.AFn.applyToHelper(AFn.java:171)"
              "at clojure.lang.AFn.applyTo(AFn.java:144)"
              "at clojure.core$apply.invokeStatic(core.clj:667)"
              "at clojure.core$apply.invoke(core.clj:662)"
              "at datahike.debug_query$wrap_traced_fn$fn__70074.doInvoke(debug_query.clj:37)"
              "at clojure.lang.RestFn.invoke(RestFn.java:482)"
              "at datahike.query$lookup_patterns.invokeStatic(query.cljc:1187)"
              "at datahike.query$lookup_patterns.invoke(query.cljc:1181)"
              "at clojure.lang.AFn.applyToHelper(AFn.java:165)"
              "at clojure.lang.AFn.applyTo(AFn.java:144)"
              "at clojure.core$apply.invokeStatic(core.clj:667)"
              "at clojure.core$apply.invoke(core.clj:662)"
              "at datahike.debug_query$wrap_traced_fn$fn__70074.doInvoke(debug_query.clj:37)"
              "at clojure.lang.RestFn.invoke(RestFn.java:457)"
              "at datahike.query$_resolve_clause_STAR_.invokeStatic(query.cljc:1299)"
              "at datahike.query$_resolve_clause_STAR_.invoke(query.cljc:1200)"
              "at clojure.lang.AFn.applyToHelper(AFn.java:160)"
              "at clojure.lang.AFn.applyTo(AFn.java:144)"
              "at clojure.core$apply.invokeStatic(core.clj:667)"
              "at clojure.core$apply.invoke(core.clj:662)"
              "at datahike.debug_query$wrap_traced_fn$fn__70074.doInvoke(debug_query.clj:37)"
              "at clojure.lang.RestFn.invoke(RestFn.java:436)"
              "at datahike.query$_resolve_clause$fn__36782.invoke(query.cljc:1307)"
              "at datahike.query_stats$update_ctx_with_stats$fn__34562.invoke(query_stats.cljc:23)"
              "at datahike.tools$timed.invokeStatic(tools.cljc:144)"
              "at datahike.tools$timed.invoke(tools.cljc:140)"
              "at datahike.query_stats$update_ctx_with_stats.invokeStatic(query_stats.cljc:23)"
              "at datahike.query_stats$update_ctx_with_stats.invoke(query_stats.cljc:19)"
              "at datahike.query$_resolve_clause.invokeStatic(query.cljc:1306)"
              "at datahike.query$_resolve_clause.invoke(query.cljc:1302)"
              "at datahike.query$_resolve_clause.invokeStatic(query.cljc:1304)"
              "at datahike.query$_resolve_clause.invoke(query.cljc:1302)"
              "at clojure.core.protocols$fn__8249.invokeStatic(protocols.clj:168)"
              "at clojure.core.protocols$fn__8249.invoke(protocols.clj:124)"
              "at clojure.core.protocols$fn__8204$G__8199__8213.invoke(protocols.clj:19)"
              "at clojure.core.protocols$seq_reduce.invokeStatic(protocols.clj:31)"
              "at clojure.core.protocols$fn__8236.invokeStatic(protocols.clj:75)"
              "at clojure.core.protocols$fn__8236.invoke(protocols.clj:75)"
              "at clojure.core.protocols$fn__8178$G__8173__8191.invoke(protocols.clj:13)"
              "at clojure.core$reduce.invokeStatic(core.clj:6886)"
              "at clojure.core$reduce.invoke(core.clj:6868)"
              "at datahike.query$solve_rule$solve__36580$fn__36581.invoke(query.cljc:864)"
              "at datahike.query_stats$update_ctx_with_stats$fn__34562.invoke(query_stats.cljc:23)"
              "at datahike.tools$timed.invokeStatic(tools.cljc:144)"
              "at datahike.tools$timed.invoke(tools.cljc:140)"
              "at datahike.query_stats$update_ctx_with_stats.invokeStatic(query_stats.cljc:23)"
              "at datahike.query_stats$update_ctx_with_stats.invoke(query_stats.cljc:19)"
              "at datahike.query$solve_rule$solve__36580.invoke(query.cljc:862)"
              "at datahike.query$solve_rule.invokeStatic(query.cljc:888)"
              "at datahike.query$solve_rule.invoke(query.cljc:855)"
              "at datahike.query$resolve_clause$fn__36785.invoke(query.cljc:1315)"
              "at datahike.query_stats$update_ctx_with_stats$fn__34562.invoke(query_stats.cljc:23)"
              "at datahike.tools$timed.invokeStatic(tools.cljc:144)"
              "at datahike.tools$timed.invoke(tools.cljc:140)"
              "at datahike.query_stats$update_ctx_with_stats.invokeStatic(query_stats.cljc:23)"
              "at datahike.query_stats$update_ctx_with_stats.invoke(query_stats.cljc:19)"
              "at datahike.query$resolve_clause.invokeStatic(query.cljc:1314)"
              "at datahike.query$resolve_clause.invoke(query.cljc:1309)"
              "at datahike.query$_resolve_clause_STAR_$fn__36749.invoke(query.cljc:1220)"
              "at clojure.core$map$fn__5935.invoke(core.clj:2772)"
              "at clojure.lang.LazySeq.sval(LazySeq.java:42)"
              "- eliminated <0x0000000674969b70> (a clojure.lang.LazySeq)"
              "at clojure.lang.LazySeq.seq(LazySeq.java:51)"
              "- locked <0x0000000674969b70> (a clojure.lang.LazySeq)"
              "at clojure.lang.RT.seq(RT.java:535)"
              "at clojure.core$seq__5467.invokeStatic(core.clj:139)"
              "at clojure.core$map$fn__5935.invoke(core.clj:2763)"
              "at clojure.lang.LazySeq.sval(LazySeq.java:42)"
              "- eliminated <0x0000000674969bc8> (a clojure.lang.LazySeq)"
              "at clojure.lang.LazySeq.seq(LazySeq.java:51)"
              "- locked <0x0000000674969bc8> (a clojure.lang.LazySeq)"
              "at clojure.lang.Cons.next(Cons.java:39)"
              "at clojure.lang.RT.next(RT.java:713)"
              "at clojure.core$next__5451.invokeStatic(core.clj:64)"
              "at clojure.core.protocols$seq_reduce.invokeStatic(protocols.clj:27)"
              "at clojure.core.protocols$fn__8236.invokeStatic(protocols.clj:75)"
              "at clojure.core.protocols$fn__8236.invoke(protocols.clj:75)"
              "at clojure.core.protocols$fn__8178$G__8173__8191.invoke(protocols.clj:13)"
              "at clojure.core$reduce.invokeStatic(core.clj:6882)"
              "at clojure.core$reduce.invoke(core.clj:6868)"
              "at datahike.query$_resolve_clause_STAR_.invokeStatic(query.cljc:1223)"
              "at datahike.query$_resolve_clause_STAR_.invoke(query.cljc:1200)"
              "at clojure.lang.AFn.applyToHelper(AFn.java:160)"
              "at clojure.lang.AFn.applyTo(AFn.java:144)"
              "at clojure.core$apply.invokeStatic(core.clj:667)"
              "at clojure.core$apply.invoke(core.clj:662)"
              "at datahike.debug_query$wrap_traced_fn$fn__70074.doInvoke(debug_query.clj:37)"
              "at clojure.lang.RestFn.invoke(RestFn.java:436)"
              "at datahike.query$_resolve_clause$fn__36782.invoke(query.cljc:1307)"
              "at datahike.query_stats$update_ctx_with_stats.invokeStatic(query_stats.cljc:31)"
              "at datahike.query_stats$update_ctx_with_stats.invoke(query_stats.cljc:19)"
              "at datahike.query$_resolve_clause.invokeStatic(query.cljc:1306)"
              "at datahike.query$_resolve_clause.invoke(query.cljc:1302)"
              "at datahike.query$_resolve_clause.invokeStatic(query.cljc:1304)"
              "at datahike.query$_resolve_clause.invoke(query.cljc:1302)"
              "at clojure.core.protocols$fn__8249.invokeStatic(protocols.clj:168)"
              "at clojure.core.protocols$fn__8249.invoke(protocols.clj:124)"
              "at clojure.core.protocols$fn__8204$G__8199__8213.invoke(protocols.clj:19)"
              "at clojure.core.protocols$seq_reduce.invokeStatic(protocols.clj:31)"
              "at clojure.core.protocols$fn__8236.invokeStatic(protocols.clj:75)"
              "at clojure.core.protocols$fn__8236.invoke(protocols.clj:75)"
              "at clojure.core.protocols$fn__8178$G__8173__8191.invoke(protocols.clj:13)"
              "at clojure.core$reduce.invokeStatic(core.clj:6886)"
              "at clojure.core$reduce.invoke(core.clj:6868)"
              "at datahike.query$solve_rule$solve__36580.invoke(query.cljc:872)"
              "at datahike.query$solve_rule.invokeStatic(query.cljc:888)"
              "at datahike.query$solve_rule.invoke(query.cljc:855)"
              "at datahike.query$resolve_clause$fn__36785.invoke(query.cljc:1315)"
              "at datahike.query_stats$update_ctx_with_stats.invokeStatic(query_stats.cljc:31)"
              "at datahike.query_stats$update_ctx_with_stats.invoke(query_stats.cljc:19)"
              "at datahike.query$resolve_clause.invokeStatic(query.cljc:1314)"
              "at datahike.query$resolve_clause.invoke(query.cljc:1309)"
              "at clojure.lang.PersistentVector.reduce(PersistentVector.java:343)"
              "at clojure.core$reduce.invokeStatic(core.clj:6885)"
              "at clojure.core$reduce.invoke(core.clj:6868)"
              "at datahike.query$_q.invokeStatic(query.cljc:1320)"
              "at datahike.query$_q.invoke(query.cljc:1318)"
              "at datahike.query$raw_q.invokeStatic(query.cljc:1458)"
              "at datahike.query$raw_q.invoke(query.cljc:1446)"
              "at datahike.query$q.invokeStatic(query.cljc:81)"
              "at datahike.query$q.doInvoke(query.cljc:75)"
              "at clojure.lang.RestFn.invoke(RestFn.java:410)"
              "at clojure.lang.AFn.applyToHelper(AFn.java:154)"
              "at clojure.lang.RestFn.applyTo(RestFn.java:132)"
              "at clojure.core$apply.invokeStatic(core.clj:667)"
              "at clojure.core$apply.invoke(core.clj:662)"
              "at datahike.debug_query$wrap_traced_fn$fn__70074.doInvoke(debug_query.clj:37)"
              "at clojure.lang.RestFn.applyTo(RestFn.java:137)"
              "at clojure.core$apply.invokeStatic(core.clj:667)"
              "at clojure.core$apply.invoke(core.clj:662)"
              "at datahike.debug_query$traced_q$fn__70125.invoke(debug_query.clj:81)"
              "at clojure.core$with_redefs_fn.invokeStatic(core.clj:7582)"
              "at clojure.core$with_redefs_fn.invoke(core.clj:7566)"
              "at datahike.debug_query$traced_q.invokeStatic(debug_query.clj:72)"
              "at datahike.debug_query$traced_q.doInvoke(debug_query.clj:69)"
              "at clojure.lang.RestFn.invoke(RestFn.java:408)"
              "at datahike.debug_query$run_example$fn__70201.invoke(debug_query.clj:239)"
              "at datahike.debug_query$with_connection_fn.invokeStatic(debug_query.clj:16)"
              "at datahike.debug_query$with_connection_fn.invoke(debug_query.clj:13)"
              "at datahike.debug_query$run_example.invokeStatic(debug_query.clj:237)"
              "at datahike.debug_query$run_example.invoke(debug_query.clj:235)"
              "at datahike.debug_query$demo0.invokeStatic(debug_query.clj:243)"
              "at datahike.debug_query$demo0.invoke(debug_query.clj:243)"
              "at datahike.debug_query$exercise_many.invokeStatic(debug_query.clj:248)"
              "at datahike.debug_query$exercise_many.invoke(debug_query.clj:245)"
              "at datahike.debug_query$exercise_many.invokeStatic(debug_query.clj:249)"
              "at datahike.debug_query$exercise_many.invoke(debug_query.clj:245)"
              "at datahike.debug_query$eval70211.invokeStatic(NO_SOURCE_FILE:18718)"
              "at datahike.debug_query$eval70211.invoke(NO_SOURCE_FILE:18718)"
              "at clojure.lang.Compiler.eval(Compiler.java:7194)"
              "at clojure.lang.Compiler.eval(Compiler.java:7149)"
              "at clojure.core$eval.invokeStatic(core.clj:3215)"
              "at clojure.core$eval.invoke(core.clj:3211)"
              "at nrepl.middleware.interruptible_eval$evaluate$fn__58126$fn__58127.invoke(interruptible_eval.clj:87)"
              "at clojure.lang.AFn.applyToHelper(AFn.java:152)"
              "at clojure.lang.AFn.applyTo(AFn.java:144)"
              "at clojure.core$apply.invokeStatic(core.clj:667)"
              "at clojure.core$with_bindings_STAR_.invokeStatic(core.clj:1990)"
              "at clojure.core$with_bindings_STAR_.doInvoke(core.clj:1990)"
              "at clojure.lang.RestFn.invoke(RestFn.java:425)"
              "at nrepl.middleware.interruptible_eval$evaluate$fn__58126.invoke(interruptible_eval.clj:87)"
              "at clojure.main$repl$read_eval_print__9206$fn__9209.invoke(main.clj:437)"
              "at clojure.main$repl$read_eval_print__9206.invoke(main.clj:437)"
              "at clojure.main$repl$fn__9215.invoke(main.clj:458)"
              "at clojure.main$repl.invokeStatic(main.clj:458)"
              "at clojure.main$repl.doInvoke(main.clj:368)"
              "at clojure.lang.RestFn.invoke(RestFn.java:1523)"
              "at nrepl.middleware.interruptible_eval$evaluate.invokeStatic(interruptible_eval.clj:84)"
              "at nrepl.middleware.interruptible_eval$evaluate.invoke(interruptible_eval.clj:56)"
              "at nrepl.middleware.interruptible_eval$interruptible_eval$fn__58159$fn__58163.invoke(interruptible_eval.clj:152)"
              "at clojure.lang.AFn.run(AFn.java:22)"
              "at nrepl.middleware.session$session_exec$main_loop__58229$fn__58233.invoke(session.clj:218)"
              "at nrepl.middleware.session$session_exec$main_loop__58229.invoke(session.clj:217)"
              "at clojure.lang.AFn.run(AFn.java:22)"
              "at java.lang.Thread.runWith(java.base@20.0.1/Thread.java:1636)"
              "at java.lang.Thread.run(java.base@20.0.1/Thread.java:1623)"],
             :pid 107042,
             :limit 174}])

(def data1 [{:header
  "\"nREPL-session-6c960671-9bbe-4749-88a8-b94a9abc539e\" #35 [107094] daemon prio=5 os_prio=0 cpu=1610300,63ms elapsed=2124,17s tid=0x00007f2448003260 nid=107094 runnable  [0x00007f24cd2ea000]",
  :state "java.lang.Thread.State: RUNNABLE",
             :stack-trace ["at clojure.lang.RT.next(RT.java:717)"
                           "at clojure.core$next__5451.invokeStatic(core.clj:64)"
                           "at clojure.core$second__5457.invokeStatic(core.clj:98)"
                           "at clojure.core$second__5457.invoke(core.clj:98)"
                           "at datahike.debug_query$openness.invokeStatic(debug_query.clj:173)"
                           "at datahike.debug_query$openness.invoke(debug_query.clj:171)"
                           "at datahike.debug_query$end_QMARK_.invokeStatic(debug_query.clj:176)"
                           "at datahike.debug_query$end_QMARK_.invoke(debug_query.clj:175)"
                           "at clojure.core$filter$fn__5958$fn__5959.invoke(core.clj:2822)"
                           "at clojure.lang.PersistentVector.reduce(PersistentVector.java:343)"
                           "at clojure.core$transduce.invokeStatic(core.clj:6946)"
                           "at clojure.core$transduce.invoke(core.clj:6933)"
                           "at datahike.debug_query$raw_acc_tree.invokeStatic(debug_query.clj:180)"
                           "at datahike.debug_query$raw_acc_tree.invoke(debug_query.clj:178)"
                           "at datahike.debug_query$acc_tree.invokeStatic(debug_query.clj:216)"
                           "at datahike.debug_query$acc_tree.invoke(debug_query.clj:215)"
                           "at datahike.debug_query$run_example$fn__70201.invoke(debug_query.clj:240)"
                           "at datahike.debug_query$with_connection_fn.invokeStatic(debug_query.clj:16)"
                           "at datahike.debug_query$with_connection_fn.invoke(debug_query.clj:13)"
                           "at datahike.debug_query$run_example.invokeStatic(debug_query.clj:237)"
                           "at datahike.debug_query$run_example.invoke(debug_query.clj:235)"
                           "at datahike.debug_query$demo0.invokeStatic(debug_query.clj:243)"
                           "at datahike.debug_query$demo0.invoke(debug_query.clj:243)"
                           "at datahike.debug_query$exercise_many.invokeStatic(debug_query.clj:248)"
                           "at datahike.debug_query$exercise_many.invoke(debug_query.clj:245)"
                           "at datahike.debug_query$exercise_many.invokeStatic(debug_query.clj:249)"
                           "at datahike.debug_query$exercise_many.invoke(debug_query.clj:245)"
                           "at datahike.debug_query$eval70211.invokeStatic(NO_SOURCE_FILE:18718)"
                           "at datahike.debug_query$eval70211.invoke(NO_SOURCE_FILE:18718)"
                           "at clojure.lang.Compiler.eval(Compiler.java:7194)"
                           "at clojure.lang.Compiler.eval(Compiler.java:7149)"
                           "at clojure.core$eval.invokeStatic(core.clj:3215)"
                           "at clojure.core$eval.invoke(core.clj:3211)"
                           "at nrepl.middleware.interruptible_eval$evaluate$fn__58126$fn__58127.invoke(interruptible_eval.clj:87)"
                           "at clojure.lang.AFn.applyToHelper(AFn.java:152)"
                           "at clojure.lang.AFn.applyTo(AFn.java:144)"
                           "at clojure.core$apply.invokeStatic(core.clj:667)"
                           "at clojure.core$with_bindings_STAR_.invokeStatic(core.clj:1990)"
                           "at clojure.core$with_bindings_STAR_.doInvoke(core.clj:1990)"
                           "at clojure.lang.RestFn.invoke(RestFn.java:425)"
                           "at nrepl.middleware.interruptible_eval$evaluate$fn__58126.invoke(interruptible_eval.clj:87)"
                           "at clojure.main$repl$read_eval_print__9206$fn__9209.invoke(main.clj:437)"
                           "at clojure.main$repl$read_eval_print__9206.invoke(main.clj:437)"
                           "at clojure.main$repl$fn__9215.invoke(main.clj:458)"
                           "at clojure.main$repl.invokeStatic(main.clj:458)"
                           "at clojure.main$repl.doInvoke(main.clj:368)"
                           "at clojure.lang.RestFn.invoke(RestFn.java:1523)"
                           "at nrepl.middleware.interruptible_eval$evaluate.invokeStatic(interruptible_eval.clj:84)"
                           "at nrepl.middleware.interruptible_eval$evaluate.invoke(interruptible_eval.clj:56)"
                           "at nrepl.middleware.interruptible_eval$interruptible_eval$fn__58159$fn__58163.invoke(interruptible_eval.clj:152)"
                           "at clojure.lang.AFn.run(AFn.java:22)"
                           "at nrepl.middleware.session$session_exec$main_loop__58229$fn__58233.invoke(session.clj:218)"
                           "at nrepl.middleware.session$session_exec$main_loop__58229.invoke(session.clj:217)"
                           "at clojure.lang.AFn.run(AFn.java:22)"
                           "at java.lang.Thread.runWith(java.base@20.0.1/Thread.java:1636)"
                           "at java.lang.Thread.run(java.base@20.0.1/Thread.java:1623)"],
             :pid 107042,
             :limit 23}])

(deftest test-parse-stack-trace-line
  (is (= {:source "at clojure.lang.RT.first(RT.java:692)"
          :file "RT.java",
          :line 692,
          :qualified-name "clojure.lang.RT.first",
          :qualified-class "clojure.lang.RT",
          :method "first",
          :classes ["clojure.lang.RT"]}
         (parse-stack-trace-line "at clojure.lang.RT.first(RT.java:692)")))
  (is (= (step-path {} [{:source :a :value 119}])
         {:sub {:a {:source :a, :value 119, :counter 1}}, :counter 1}))
  (is (= (step-path {} [{:source :a :value 119} {:source :b :value 120}])
         {:sub
          {:a
           {:source :a,
            :value 119,
            :sub {:b {:source :b, :value 120, :counter 1}},
            :counter 1}},
          :counter 1})))


(defn demo []
  (run-tests))
