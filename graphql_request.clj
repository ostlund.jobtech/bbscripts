(ns graphql-request
  (:require [babashka.http-client :as http]))

(defn run-graphql [url]
  (http/get url {:headers {"Accept" "application/json"} :throw false}))

(defn ex0 []
  (run-graphql "http://taxonomy-t2.api.jobtechdev.se/v1/taxonomy/graphql?query=%7B%0Aconcepts%28type%3A%20%22occupation-field%22%2C%20version%3A%20%221%22%29%20%7B%0A%20%20%20id%0A%20%20%20name%3A%20preferred_label%0A%20%20%20yrkesgrupper%3A%20narrower%28type%3A%20%22ssyk-level-4%22%29%20%7B%0A%20%20%20%20%20%20id%0A%20%20%20%20%20%20name%3A%20preferred_label%0A%20%20%20%7D%0A%7D"))

(defn ex1 []
  (run-graphql "http://taxonomy-t2.api.jobtechdev.se:443/v1/taxonomy/graphql?query=%7B%0Aconcepts%28type%3A%20%22occupation-field%22%2C%20version%3A%20%221%22%29%20%7B%0A%20%20%20id%0A%20%20%20name%3A%20preferred_label%0A%20%20%20yrkesgrupper%3A%20narrower%28type%3A%20%22ssyk-level-4%22%29%20%7B%0A%20%20%20%20%20%20id%0A%20%20%20%20%20%20name%3A%20preferred_label%0A%20%20%20%7D%0A%7D"))
