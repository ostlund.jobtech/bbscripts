#!/usr/bin/env bb

;; How to run it:
;;
;; 0. ln -s /home/jonas/prog/bbscripts/completion/demo.clj /home/jonas/local/bin/demo.clj
;; 1. `source demo-completion.bash`
;; 2. `./demo.clj ....<tab>`

(ns demo
  (:require [clojure.test :refer [deftest is run-tests]]
            [babashka.process :refer [shell *defaults*]]
            [clojure.string :as str]
            [clojure.pprint :refer [pprint]]))

(def empty-state {:files []
                  :types []
                  :state-key :top})

(defn compgen [args]
  (shell {:in (str "compgen " (str/join " " args))
          :out :string :err :string}
         "bash"))

(defn compgen-out [args]
  (try
    (-> args
        compgen
        :out
        println)
    (catch Exception _e
      nil)))

(defn interpret [state input]
  (merge
   state
   (case (:state-key state)
     :top (case input
            "file" {:state-key :file}
            "type" {:state-key :type})
     :file (-> state
               (update :files conj input)
               (assoc :state-key :top))
     :type (-> state
               (update :types conj (case input
                                     "json" :json
                                     "yaml" :yaml))
               (assoc :state-key :top)))))

(deftest compgen-test
  (is (= (:out (compgen ["-W" "'cat dog'" "c"]))
         "cat\n"))
  (is (thrown? Exception
               (compgen-out ["-W" "'json yaml'" "."]))))

(deftest interpret-test
  (is (= {:files ["katt.json"]
          :types []
          :state-key :top}
         (reduce interpret empty-state ["file" "katt.json"])))
  (is (= {:files ["katt.json"]
          :types []
          :state-key :type}
         (reduce interpret empty-state ["file" "katt.json" "type"])))
  (is (= {:files ["katt.json"]
          :types [:yaml]
          :state-key :top}
         (reduce interpret empty-state ["file" "katt.json" "type" "yaml"]))))

(defn demo []
  (run-tests))

(defn perform-completion [args]
  (let [state (reduce interpret empty-state (butlast args))
        word (last args)]
    (spit "mjao.edn" (pr-str {:state state
                              :word word}))
    (case (:state-key state)
      :top (compgen-out ["-W" "'file type'" word])
      :type (compgen-out ["-W" "'json yaml'" word])
      :file (compgen-out ["-f" word]))))

(defn -main [& args]
  (spit "args.edn" (pr-str args))
  (if (= "**complete**" (first args))
      (perform-completion (drop 2 args))
      (pprint (reduce interpret empty-state args))))

(apply -main *command-line-args*)
