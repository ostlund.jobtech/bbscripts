#/usr/bin/env bash
_demo_completions()
{
  COMPREPLY=($(./demo.clj **complete** "${COMP_WORDS[@]}"))
}
complete -F _demo_completions demo.clj
