(ns doctasks
  (:require [clojure.string :as str]))

(defn task-range [lower upper]
  (fn [index]
    (<= lower index upper)))

(def docs [["https://gitlab.com/arbetsformedlingen/taxonomy-dev/secret-documents/-/blob/main/meetings/20240430-doc-planning3.md" (task-range 30 38)]
           ["https://gitlab.com/arbetsformedlingen/taxonomy-dev/secret-documents/-/blob/main/meetings/20240502-doc-planning4.md" (task-range 39 46)]
           ["https://gitlab.com/arbetsformedlingen/taxonomy-dev/secret-documents/-/blob/main/meetings/20240503-doc-planning5.md" (task-range 47 54)]])

(defn get-doc [task-index]
  {:pre [(number? task-index)]
   :post [(string? %)]}
  (first (keep (fn [[label pred]] (when (pred task-index) label)) docs)))

(defn render-task-spec [inds]
  (println "Update the documentation according to these tasks:\n")
  (doseq [[task-link inds] (sort-by first (group-by get-doc inds))]
    (let [inds (sort inds)]
      (println (format "* [%s](%s)"
                       (str/join ", " (map #(format "TASK-%d" %) inds))
                       task-link)))))
